import subprocess
import os
from docx import Document
import pandas as pd
import cv2
pd.set_option('display.max_columns', None)  # or 1000
pd.set_option('display.max_rows', None)  # or 1000
pd.set_option('display.max_colwidth', -1)
import pytesseract
from pytesseract import image_to_string


pytesseract.pytesseract.tesseract_cmd = '/home/manojy/anaconda/bin/tesseract'



def preprocessDoc(file):
    print(file)
    directory = os.path.dirname(file)
    ss= subprocess.call(['soffice','--convert-to','docx','--outdir',directory,str(file)])
    data = []
    marked = False
    # read docx file and return pandas dataframe
    if os.path.basename(file)[0] == "~":
        pass
    else:
        try:
            doc = Document(str(file).replace('doc', 'docx'))
            str_data = ''

            for i in doc.paragraphs:
                if ":" not in i.text and len(i.text)>50 and not marked:
                    if ":" in str_data:
                        str_data+=i.text+"$"+"\n"
                        continue
                    str_data+="HISTORY:"+i.text+" $\n"
                    marked = True
                else:

                    str_data += i.text+" $"+'\n'
            data = str_data.split('\n')
        except Exception as ex:
            print(ex)
            pass
    # with open('out_doc.csv','a') as fi:
    #     fi.write(data)
    return data

def preprocessImage(file):
    # process image here
    print(file)
    img = cv2.imread(file, 0)
    # print(img.shape)

    tesstext = image_to_string(img)
    tesstext = tesstext.replace("\n","  $\n\n")

    tesstext = tesstext.replace('#2',":")
    data = tesstext.split('\n')
    return data
