# import os
# projectdir = '/project7'
# datadir = '/datatemp'
# data = '/datasets'
# targetdir = '/targetdata'
# currentpath = '/home/manojy/Desktop/ManojY/nlp_ehr/Categoriser'
# datapath = 'datasets/'
# mainpath = currentpath + projectdir + datadir
# # all_files =[]
# import utilities as ui
# # for root,dir,files in os.walk(mainpath):
# #
# #     # print(root)
# #     # print(dir)
# #     # print(files)
# #     for file in files:
# #
# #         if file.split('.')[1]=="doc" or file.split('.')[1]=="cmp":
# #             print(file)
# #             all_files.append(file)
# #
# # print(len(all_files))
#
# count = 0
# all_files = ui.getAllFiles(mainpath)
# print(len(all_files))
# for file in all_files:
#     file =str(file)
#     if os.path.basename(file).split('.')[1]=="doc":
#         count+=1
#
# print(count)
#


import os
import pyinotify


class EventProcessor(pyinotify.ProcessEvent):
    _methods = ["IN_CREATE",


                "IN_DELETE",
                "IN_DELETE_SELF",

                "IN_MODIFY",
                "IN_MOVE_SELF",
                "IN_MOVED_FROM",
               ]
all_files=[]
def process_generator(cls, method):
    def _method_name(self, event):
        print("Method name: process_{}()\n"
               "Path name: {}\n"
               "Event Name: {}\n".format(method, event.pathname, event.maskname))

        all_files.append([event.pathname,event.maskname])

    _method_name.__name__ = "process_{}".format(method)
    setattr(cls, _method_name.__name__, _method_name)
    print(all_files)

for method in EventProcessor._methods:
    process_generator(EventProcessor, method)

watch_manager = pyinotify.WatchManager()
event_notifier = pyinotify.Notifier(watch_manager, EventProcessor())

watch_this = os.path.abspath("/home/manojy/Desktop/ManojY/Practice")
watch_manager.add_watch(watch_this, pyinotify.ALL_EVENTS)
event_notifier.loop()