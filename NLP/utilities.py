import os
from pathlib import Path
import re
import pandas as pd
import constant as cnt
import datetime

def getAllFiles(dir):
    dirPath = Path(dir)

    assert (dirPath.is_dir())
    file_list = []
    for x in dirPath.iterdir():
        if x.is_file():
            file_list.append(x)
        elif x.is_dir():
            file_list.extend(getAllFiles(x))
    return (file_list)


def clean(dataset):
    processData=[]
    for x in range(len(dataset)):
        # text=str(dataset.iloc[x])
        # if dataset.iloc[x][0]=="":
        #
        #     dataset.drop(dataset.index[x])
        text = dataset[x]
        text=text.lstrip("0")
        text=text.strip()
        text=text.replace('\\n',' ')
        text = text.replace('.',' ')
        text = re.sub(r'[; @`]',' ',text)
        text = re.sub(r'\s+',' ',text)
        dtime = re.search('\d{2}:\d{2}:\d{2}', text)
        try:
            text = text.replace(dtime.group(),dtime.group().replace(':',"-"))
        except:
            try:
                dtime = re.search('\d{1}:\d{2}:\d{2}', text)
                text = text.replace(dtime.group(), dtime.group().replace(':', "-"))
            except:
                pass

            pass
        # subject = r"dd\:dd\:dd"
        #
        # match = re.search(r"field2:\s(.*)", subject)
        # match = re.search(r"field2:\s(.*)", subject)
        if text!='':

            processData.append(text)

    # print(type(processData))
    return processData

def getVitals(filename,date):
    df = pd.read_excel(filename, 'Vitals')
    for i in df.itertuples():
        try:

            if i.entryDate.date() == date.date():
                data_dict = {}
                for index, j in enumerate(i):
                    if index == 0:
                        continue
                    data_dict[df.columns[index - 1].replace('.', '_')] = str(j)
                return data_dict
        except:
            if i.entrydate.date() == date.date():
                data_dict = {}
                for index, j in enumerate(i):
                    if index == 0:
                        continue
                    data_dict[df.columns[index - 1].replace('.', '_')] = str(j)

            # data.append(data_dict)
                return data_dict
    data_dict={}
    return data_dict


def getDate(filename,inp_file):
    df = pd.read_excel(filename,'ChartFiles')
    for i in df.itertuples():
        data_dict = {}
        if i.filename ==inp_file:
            return i.dos




def getDemographics(filename):
    # excel = pd.ExcelFile(filename)

    df = pd.read_excel(filename,'Demographics')
    for i in df.itertuples():
        data_dict = {}
        for index, j in enumerate(i):
            if index == 0:
                continue
            data_dict[df.columns[index - 1].replace('.', '_')] = str(j)


         # data.append(data_dict)
        return data_dict
    data_dict={}
    return data_dict



def markExceptions(processData):
    for i in cnt.exceptionValues:
        if len(i.split(" "))>1:
            wrd = re.compile(re.escape(i), re.IGNORECASE)
            i = '_'.join(i.split(" "))
            processData = wrd.sub(i + " :", processData)
        else:

            wrd = re.compile(re.escape(i),re.IGNORECASE)
            processData  = wrd.sub(i+" :",processData)

    return  processData



def getDateTime(text):



    try:
        month, date, year = text.split("/")
        dt = datetime.datetime(int(year), int(month), int(date))
        return dt
    except:
        return False
if __name__=="__main__":
    print(os)