import re
import datetime
import constant as cnt

from scipy.sparse.data import _data_matrix


def checkDate(text):



    try:
        month, date, year = text.split("/")
        dt = datetime.datetime(int(year), int(month), int(date))
        return True
    except:
        return False


def extractPageNo(data):
    pagerng = re.findall(r'page\s\d+\sof+\s\d',data.lower(),re.IGNORECASE)
    pgeno= re.findall(r'page\s\d',data,re.IGNORECASE)

    page = re.findall(r'page',data,re.IGNORECASE)
    pgNo={}
    print(pagerng)
    for i in pagerng:
        numbers = re.findall('\d',i)
        pgNo["start"]=numbers[0]
        pgNo["end"]=numbers[1]

    if pagerng==[] and pgeno!=[]:

        numbers = re.findall('\d',pgeno[0])
        pgNo["start"]=0
        pgNo["end"]=numbers[0]
    if page!=[]:
        print(page)
    return pgNo




def processClinicalReport(data,filename):
    if filename=="00000000946320050516134924.cmp":
        print(filename)
    listdataOut = []
    dataOut={}
    pageNo = extractPageNo(data)


    matchdate = re.findall(r'(\d+/\d{2}/\d+)', data)

    try:

        for i in matchdate:
            data = data.replace(i,"^"+i+"^")

    except Exception as ex:
        print(ex)

    print("---------------------------")
    print(data.split("^"))
    key = ""
    for text in data.split("^"):
        if checkDate(text):
            if "date" in dataOut:
                listdataOut.append(dataOut)
                dataOut={}
                dataOut["date"]= text
                key= ""
            else:
                dataOut["date"]= text
        else:
            text.split("$")
            for line in text.split("$"):
                if line =="":
                    continue
                if ":" in line:
                    line = re.sub(":","^",line)
                    key = line.split("^")[0].strip().lower()
                    dataOut[key]=line.split("^")[1]
                else:
                    for keyword in cnt.constant:
                        try:

                            ss =re.search(keyword,line).group()
                        except:
                            continue
                        if ss:
                            key = keyword.strip().lower()
                            try:

                                dataOut[key]=line.split(keyword)[1]
                            except:
                                pass
                    if key=="":
                        if filename.split('.')[1]=="cmp":


                            key = "background"
                            dataOut[key] = line
                        else:
                            key = "misc"
                            dataOut[key]=line
                    else:
                        dataOut[key]=dataOut[key]+"  "+line
    if dataOut not in listdataOut and dataOut:
        listdataOut.append(dataOut)
    return  listdataOut



def processOperativeNote(data,filename):
    listdataOut = []
    dataOut = {}
    pageNo = extractPageNo(data)
    if pageNo:
        dataOut["pgstart"]=pageNo["start"]
        dataOut["pgend"]=pageNo["end"]






    matchdate = re.findall(r'(\d+/\d+/\d+)', data)

    try:
        prevDate = datetime.datetime(int(1), int(1), int(1))
        for i in matchdate:
            try:
                month, date, year = i.split("/")
                dt = datetime.datetime(int(year), int(month), int(date))

            except:
                continue


            if dt!=prevDate and dt >prevDate:

                data = data.replace(i, "^" + i + "^")

                prevDate = dt
            # else:



    except Exception as ex:
        print(ex)

    print("---------------------------")
    print(data.split("^"))
    key = ""
    for text in data.split("^"):
        if checkDate(text):
            if "date" in dataOut and dataOut["date"] != text:
                listdataOut.append(dataOut)
                dataOut = {}
                dataOut["date"] = text
                key = ""
            else:
                dataOut["date"] = text
        else:
            text.split("$")
            for line in text.split("$"):
                if line == "":
                    continue
                if ":" in line:
                    line = re.sub(":", "^", line)
                    key = line.split("^")[0].strip().lower()
                    dataOut[key] = line.split("^")[1]
                else:
                    for keyword in cnt.constant:
                        try:

                            ss =re.search(keyword,line,re.IGNORECASE).group()
                        except:
                            continue
                        if ss:
                            key = keyword.strip().lower()
                            try:

                                dataOut[key]=line.split(keyword)[1]
                                continue
                            except:
                                dataOut[key]=""
                                # pass
                                continue
                    if key == "":
                        key = "misc"
                        dataOut[key] = line
                    else:
                        dataOut[key] = dataOut[key] + "  " + line
    if dataOut not in listdataOut and dataOut:
        listdataOut.append(dataOut)
    return listdataOut



def processData(data,filename):
    outres = {}

    try:
        pexam = re.findall(r'\bphysical exam\b',data,re.IGNORECASE)
        for i in pexam:
            data = data.replace(i + " $", i + ":")

        # print(data)
    except:
        pass



    dtime = re.search('\d+/\d+/\d+', data)

    try:
        data =data.replace(dtime.group(),dtime.group().replace(':',"-"))
    except:
        dtime = re.search('\d:\d{2}', data)
        try:
            data = data.replace(dtime.group(), dtime.group().replace(':', "-"))
        except:

            pass

    # replace dates btw paragraphs
    try:
        dtimepara = re.findall(r'on\s\d+/\d{2}/\d+',data)[0]
        for i in dtimepara:

            data = data.replace(i,i.replace('/','-'))
        print(dtimepara)
    except Exception as ex:
        print(ex)


    pattern = "operative note"
    ss = re.search(pattern,data,re.IGNORECASE)
    try:
        ss.group()
        outdata = processOperativeNote(data,filename)
    except Exception as ex:
        pattern = "discharge"
        ss = re.search(pattern,data,re.IGNORECASE)
        try:
            ss.group()
            outdata=processOperativeNote(data,filename)
        except:
            outdata=processClinicalReport(data,filename)



    # data = data.split("$")
    # for i in data:
    #     if ":" in i:
    #         res = i.split(":")
    #         outres[res[0]]=res[1]
    #         print(i)
    #     else:
    #         try:
    #
    #
    #             if "misc" in outres:
    #                 outres["misc"]= outres["misc"]+"\n"+i
    #
    #             else:
    #                 outres["misc"] = i
    #         except Exception as ex:
    #             print(ex)

            # print("====")

    return outdata
