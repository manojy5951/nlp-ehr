from nltk.tag import StanfordNERTagger
from nltk.tokenize import word_tokenize

st = StanfordNERTagger('/home/manojy/Desktop/ManojY/nltk_ner/stanford-ner-2018-10-16/classifiers/english.all.3class.distsim.crf.ser.gz',
					   '/home/manojy/Desktop/ManojY/nltk_ner/stanford-ner-2018-10-16/stanford-ner.jar',
					   encoding='utf-8')

text = 'While in France, Christine Lagarde discussed short-term stimulus efforts in a recent interview with the Wall Street Journal.'

tokenized_text = word_tokenize(text)
classified_text = st.tag(tokenized_text)

print(classified_text)


# import nltk
#
# wrds = "hi_thid_id msnoj"
# ssd = nltk.word_tokenize(wrds)
# print(ssd)
#
# # ar = [5,7,4,6,4,7]
# # n = 15
# # out =[]
# # for i in range(n):
# #
# #     for j in range(len(ar)):
# #         out.append(ar[j])
# #     ar.reverse()
# # print(out[0:n])
#
# # ar = [1,2,3,4]
# # arr = ar.reverse()
# # n= 10
# #
# # j=1
# # for i in range(n):
# #     if i%len(ar)==0:
# #         print(ar[i])
# #     else:
# #         print(arr[i])
#     #
#     # if i<len(ar):
#     #     print(ar[i])
#     # else:
#     #     print(arr[i-j])
#     #     j+=1
#
# #
# # from hashlib import sha512
# # import base64
# #
# # my_file_name="myfilenameverylongtohide.myextension"
# # my_file_name = my_file_name.encode()
# # my_file_name1="myfilenameverylongtohide.myextension"
# # my_file_name1 = my_file_name1.encode()
# #
# # hash_filename= sha512(my_file_name).hexdigest()
# # hash_filename1= sha512(my_file_name1).hexdigest()
# #
# # print(hash_filename)
# # print(hash_filename1)
#
#
# # import webbrowser
# # import pyinotify
# #
# # class ModHandler(pyinotify.ProcessEvent):
# #     # evt has useful properties, including pathname
# #     def process_IN_CLOSE_WRITE(self, evt):
# #             print("changed")
# #
# # handler = ModHandler()
# # wm = pyinotify.WatchManager()
# # notifier = pyinotify.Notifier(wm, handler)
# # wdd = wm.add_watch('/home/manojy/Desktop/ManojY/Practice', pyinotify.IN_CLOSE_WRITE,rec=True)
# # notifier.loop()
#
# #
# # import os
# # import pyinotify
# #
# #
# # class EventProcessor(pyinotify.ProcessEvent):
# #     _methods = ["IN_CREATE",
# #
# #
# #                 "IN_DELETE",
# #                 "IN_DELETE_SELF",
# #
# #                 "IN_MODIFY",
# #                 "IN_MOVE_SELF",
# #                 "IN_MOVED_FROM",
# #                ]
# #
# # def process_generator(cls, method):
# #     def _method_name(self, event):
# #         print("Method name: process_{}()\n"
# #                "Path name: {}\n"
# #                "Event Name: {}\n".format(method, event.pathname, event.maskname))
# #     _method_name.__name__ = "process_{}".format(method)
# #     setattr(cls, _method_name.__name__, _method_name)
# #
# # for method in EventProcessor._methods:
# #     process_generator(EventProcessor, method)
# #
# # watch_manager = pyinotify.WatchManager()
# # event_notifier = pyinotify.Notifier(watch_manager, EventProcessor())
# #
# # watch_this = os.path.abspath("/home/manojy/Desktop/ManojY/Practice")
# # watch_manager.add_watch(watch_this, pyinotify.ALL_EVENTS)
# # event_notifier.loop()
#
#
# st = "abba"
#
#
