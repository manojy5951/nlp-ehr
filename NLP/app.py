import os
import re
import pandas as pd
from io import BytesIO
import numpy as np
from flask import Flask,request,jsonify,render_template,send_file,Session
from flask_cors import CORS
import flask_excel as excel
import xlwt
import xlsxwriter
CWD = os.getcwd()
app = Flask(__name__ ,template_folder=CWD)
CORS(app)
import pymongo
from pymongo import MongoClient
client = MongoClient()
db = client.nlp_ehr
collection = db['Final_Records']
record_key = db['Final_Records_keys']
demographics = db['Demographics']
vitals = db['Vitals']
meds = db['Meds']
#
# FileId = ''
# keyWord = ''
# searchText = ''
# sortOrder = ''
# srorder = ''
# global MAIN_RESULT
# list_imp_keys =['FileId',"physician","docdate","diagnosis","cheif_complaint","past_medical_history","past_surgical_history","social_history","history","operative_note","operation","examination","procedure","medication","recommendation","gait","motor","components","patno","dob","sex","race","height","weight","systolic","diastolic","pulse"]
# list_imp_keys=["patno","docdate","diagnosis","impression","medications","recommendation","gait","motor","components","patno","dob","sex","race","height","weight","systolic","diastolic","pulse"]
list_imp_keys =['FileName','FileId',"docdate","physician","dob","sex","race","height","weight","systolic","diastolic","pulse","medical_history","surgical_history","social_history","family_history","history_of_present_illness","history","diagnosis","postoperative","preoperative_diagnosis","medication","recommendation","procedure","impression","examination","operation","operative_note","gait","motor","component","rays","allergies","review_of_systems"]
# list_imp_keysss =['FileName','Patient #',"Date","Physician","DOB","Sex","Race","Height","Weight","Systolic","Diastolic","Pulse","Medical History","Surgical History","Social History","Family History","HPI","History","Diagnosis","Postoperative Diagnosis","Preoperative Diagnosis","Medications","Recommendation","Procedure","Impression","Exam","Operation","Procedure in Detail","Gait","Motor","Components","X-Rays","Allergies","Review Of Systems"]
# dic_imp_key={}
# for i in list_imp_keys:
#
#     # if i =="social"
#     cur = record_key.find({"value" : {'$regex' : ".*"+i+".*"}})
#     if i not in dic_imp_key:
#         dic_imp_key[i]=[]
#     if i == "operation":
#         dic_imp_key[i].append("operation_performed")
#         dic_imp_key[i].append("operation")
#
#         continue
#     if i =="history_of_present_illness":
#         dic_imp_key[i].append("hpi")
#     for val in cur:
#         if i =="history" and val["value"]!="past_medical_history" and val["value"]!="past_surgical_history":
#             continue
#
#         if val["value"] not in list_imp_keys:
#
#             dic_imp_key[i].append(val["value"])
#         if val["value"]==i:
#             dic_imp_key[i].append(val["value"])
#     if i =="physician":
#         cur = record_key.find({"value": {'$regex': ".*by.*"}})
#         for val in cur:
#             if val not in dic_imp_key[i]:
#                 dic_imp_key[i].append(val["value"])
#         dic_imp_key[i].append("surgeon")
#     if i == "recommendation":
#         cur= record_key.find({"value":{'$regex':'.*plan.*'}})
#         for val in cur:
#             if val not in dic_imp_key[i]:
#
#                 dic_imp_key[i].append(val["value"])
#     if i=="diagnosis":
#         cur = record_key.find({"value": {'$regex': '.*assessment.*'}})
#         for val in cur:
#             if val not in dic_imp_key[i]:
#                 dic_imp_key[i].append(val["value"])
#     if i == "examination":
#         cur = record_key.find({"value":{'$regex':'.*exam.*'}})
#         for val in cur:
#             if val not in dic_imp_key[i]:
#                 dic_imp_key[i].append(val["value"])
#         dic_imp_key[i].append("ination")
#     if i =="height":
#         dic_imp_key[i].append("ht")
#
#     if i =="weight":
#         dic_imp_key[i].append("wt")
#
#     if i =="pulse":
#         dic_imp_key[i].append("p")
#
#     if i =="bloodpressure":
#         dic_imp_key[i].append("bp")
#     if i =="docdate":
#         dic_imp_key[i].append("date")
#         dic_imp_key[i].append("datetemp")
#     if i =="component":
#         dic_imp_key[i].append("hardware")
#     if i =="operative_note":
#         dic_imp_key[i].append("operation_in_detail")
#         dic_imp_key[i].append("description_of_operation")
#         dic_imp_key[i].append("qvi_description_of_operation")
#         dic_imp_key[i].append("surgery_in_detail")
#     if i == "impression":
#         dic_imp_key[i].append("extremeties")
#         dic_imp_key[i].append("tmpression")
#     if i =="review_of_systems":
#         dic_imp_key[i].append("ros")
#
#         # dic_imp_key[]
#
#

import pandas as pd

dic_imp_key = pd.read_excel('column_merge.xlsx')
list_imp_keysss=[]
for key in dic_imp_key:
    list_imp_keysss.append(key)


keys_query={}
for i in list_imp_keys:
    if(i=="_id"):
        keys_query[i]=0
    else:
        keys_query[i]=1

def preprocessRes(res):
    # for dic_key in dic_imp_key:
    #     print()
    out_res = []
    for result in res:
        dict_result = {}
        for dic_key in dic_imp_key:

            dict_result[dic_key]=''
            for i in dic_imp_key[dic_key]:
                # if 'components' in result:
                #     print("found")
                if i in result:
                    # if dict_result[dic_key]=="\nNone":
                    #     dict_result[dic_key]==result[i]
                    #     # print("none")
                    # else:
                    if not str(result[i]) == "None":


                        # if not dict_result[dic_key]== str(result[i]):
                        dict_result[dic_key]=dict_result[dic_key]+"\n"+str(result[i])
                        # dict_result[dic_key]= dict_result[dic_key].


                    # print(result[i])
        res_array=[]
        for dict in dict_result:
            if dict=="physician":
                match = re.search(r'(\d+/\d+/\d+)', dict_result[dict])
                try:
                    ss = match.groups()
                    for i in ss:

                        dict_result[dict] = dict_result[dict].split(i)[1]

                except:
                    pass
            res_array.append(dict_result[dict])

        out_res.append(res_array)
        # print('---------------------------')
    # print(res)
    return out_res



@app.route('/')
def index():
    return render_template('index.html')


def createExcel(res):
    try:

        os.remove('out.xlsx')
    except:
        pass
    workbook = xlsxwriter.Workbook('out.xlsx')
    worksheet = workbook.add_worksheet()
    lis = []
    list_demo = res['demographics'][0].keys()
    for i in res['demographics']:
         list_xc=[]
         for j in i:
             list_xc.append(i[j])
         lis.append(list_xc)

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    worksheet.write(row,1,"DEMOGRAPHICS")


    row+=1
    demo_keys = res['demographics'][0].keys()
    for i,val in enumerate(demo_keys):
        worksheet.write(row,i,val)
    row+=1

    for i,val in enumerate(lis):
        for j,value in enumerate(val):
            worksheet.write(row,j,value)
        row+=1


    row+=1


    lis = []
    list_demo = res['vitals'][0].keys()
    for i in res['vitals']:
        list_xc = []
        for j in i:
            list_xc.append(i[j])
        lis.append(list_xc)



    worksheet.write(row, 1, "VITALS")

    row += 1
    demo_keys = res['vitals'][0].keys()
    for i, val in enumerate(demo_keys):
        worksheet.write(row, i, val)
    row += 1

    for i, val in enumerate(lis):
        for j, value in enumerate(val):
            worksheet.write(row, j, value)
        row += 1



    row+=1
    row+=1

    lis = []
    list_demo = res['meds'][0].keys()
    for i in res['meds']:
        list_xc = []
        for j in i:
            list_xc.append(i[j])
        lis.append(list_xc)



    worksheet.write(row, 1, "meds")

    row += 1
    demo_keys = res['meds'][0].keys()
    for i, val in enumerate(demo_keys):
        worksheet.write(row, i, val)
    row += 1

    for i, val in enumerate(lis):
        for j, value in enumerate(val):
            worksheet.write(row, j, value)
        row += 1

    worksheet.write(row, 1, "RECORDS")

    row+=1

    demo_keys= []
    for list in res['result']:
        keys = list.keys()
        for i in keys:
            if i not in demo_keys:
                demo_keys.append(i)

    for i, val in enumerate(demo_keys):
        worksheet.write(row, i, val)
    row += 1

    for list in res['result']:
        for i,j in enumerate(demo_keys):
            if j in list:
                worksheet.write(row,i,list[j])
            else:
                worksheet.write(row,i,"N/A")

        row+=1





    workbook.close()




    print(res)


@app.route('/search',methods=['GET','POST'])
def Search():
    result = []
    myQuery= {}
    data = request.form
    FileId = data['patientId']
    keyWord = data['keyword']
    searchText = data['searchText']
    sortOrder = data['sorting']
    srorder = pymongo.ASCENDING

    if(sortOrder!="Ascending"):
        srorder = pymongo.DESCENDING



    if FileId=="All":
        if keyWord == "All":
            if  searchText=='':
                cursor = collection.find({},{'_id':0})
                for doc in cursor:
                    # result.append(doc)
                    # cr = db['ChartFiles'].find({'filename':doc['FileName']})
                    # for d in cr:
                    #     doc["Date"]=d["dos"]
                    result.append(doc)
                # print(result)
                dem_cur = demographics.find({},{'_id':0})
                dem_res =[]
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)

                res = {}
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics']= dem_res
                res['vitals'] = vit_res
                res['meds']= med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])
                return jsonify(res)
            else:
                # collection.create_index("$**")
                cursor = collection.find({'$text':{'$search':searchText}}, {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)

                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])
                return jsonify(res)
        else:
            if searchText=='':

                cursor = collection.find({keyWord:{'$exists':'true'}}, {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)

                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])
                return jsonify(res)
            else:
                cursor = collection.find({keyWord:{'$exists':'true'},'$text': {'$search': searchText}},  {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)

                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                res['result'] = preprocessRes(res["result"])
                # createExcel(res)
                return jsonify(res)
    else:
        if keyWord=="All":
            if searchText=='':
                cursor = collection.find({'FileId':FileId}, {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)
                res = {}

                dem_cur = demographics.find({'FileId':FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId':FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId':FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])
                return jsonify(res)
            else:
                cursor = collection.find({'FileId': FileId},{'$text':{'$search':searchText}},  {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)
                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])

                return jsonify(res)
        else:
            if searchText=='':

                cursor = collection.find({'FileId':FileId,keyWord:{'$exists':'true'}}, {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)

                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])

                return jsonify(res)
            else:
                cursor = collection.find({keyWord:{'$exists':'true'},'$text': {'$search': searchText}},  {'_id': 0}).sort("Date",srorder)
                for doc in cursor:
                    result.append(doc)

                res = {}
                dem_cur = demographics.find({'FileId': FileId}, {'_id': 0})
                dem_res = []
                for cur in dem_cur:
                    dem_res.append(cur)
                vit_cur = vitals.find({'FileId': FileId}, {'_id': 0})
                vit_res = []
                for cur in vit_cur:
                    vit_res.append(cur)
                med_cur = meds.find({'FileId': FileId}, {'_id': 0})
                med_res = []
                for cur in med_cur:
                    med_res.append(cur)
                res["title"] = list_imp_keysss
                res["result"] = result
                res['demographics'] = dem_res
                res['vitals'] = vit_res
                res['meds'] = med_res
                # createExcel(res)
                res['result'] = preprocessRes(res["result"])

                return jsonify(res)








@app.route('/sort',methods = ['GET','POST'])
def Sort():
    result = []
    sortField = request.form["sortfield"]

    cursor = collection.find({sortField:{'$exists':'true'}},{ '_id': 0}).sort(sortField,-1)
    for doc in cursor:
        result.append(doc)

    return jsonify(result)




@app.route('/allPatients',methods=['GET'])
def getAllPatients():
    all_collections = db.collection_names()
    print(all_collections)
    cursor = collection.distinct("FileId")
    resultFile = []
    for doc in cursor:
        print(doc)
        resultFile.append(doc)

    cursor = record_key.distinct("value")
    resultKey = []
    for doc in cursor:
        print(doc)
        resultKey.append(doc)

    result = {}
    result["pId"]= resultFile
    result["keys"] = resultKey
    result['collections'] = all_collections
    return jsonify(result)


@app.route('/getkeys/<pid>')
def getkeys(pid):
    if pid=="All":
        cursor = record_key.distinct("value")
        resultKey = []
        for doc in cursor:
            print(doc)
            resultKey.append(doc)

        return jsonify(resultKey)



    cursor = collection.find({'FileId':pid})
    keys = []
    for i in cursor:
        for key in i:
            if key not  in keys:
                keys.append(key)




    return jsonify(keys)


@app.route('/exportExcel',methods=['GET'])
def exportExcel():


    # finally return the file
    return send_file('out.xlsx', attachment_filename="testing.xlsx", as_attachment=True)
    # return excel.make_response_from_array([[1, 2], [3, 4]], "csv",file_name="export_data")



if __name__ == '__main__':

    HOST = os.environ.get('IP','0.0.0.0')
    PORT = int(os.environ.get('PORT',5010))
    app.run(host=HOST,port=PORT)


    app.run()