# main module for nlp_ehr

import pandas as pd
import os
import pathlib
import utilities as ui
import preprocessing as psg
import dataExtraction as de
from pymongo import MongoClient
import processExcels
import re


client = MongoClient()

db = client.nlp_ehr
collection = db["Records_test"]
fi_collection = db["Final_Records"]

pd.set_option('display.max_columns', None)  # or 1000
pd.set_option('display.max_rows', None)  # or 1000
pd.set_option('display.max_colwidth', -1)

projectdir = '/project7'
datadir = '/datatemp'
data = '/datatemp'
targetdir = '/targetdata'
currentpath = '/home/manojy/Desktop/ManojY/nlp_ehr/Categoriser'
datapath = 'datatemp/'
mainpath = currentpath + projectdir + datadir
currentdir = pathlib.Path(mainpath)
print('mainpath, data, currentdir: ', mainpath, data, currentdir)

files = ui.getAllFiles(currentdir)


def sortFiles(files):
    listData = []
    for i in files:
        filedata = {}
        key = os.path.basename(str(i))
        if key.split('.')[1]=="xlsx"  or key.split('.')[1]=="db":
            continue
        else:
            # filedata[key.split(".")[0]]= i
            filedata["Key"]= key.split(".")[0]
            filedata["Value"]= i
            listData.append(filedata)


    df = pd.DataFrame(listData)
    df =df.sort_values("Key")

    # df.sort_index()
    # print(df)

    return df["Value"]




excel_files = [i for i in files if str(i).split('.')[1]=="xlsx"]
def process(all_files):

    records =[]

    all_files = sortFiles(all_files)

    hashFiles = []
    for file in all_files:
        file = str(file)
        dir = os.path.dirname(file)

        fileId = os.path.basename(dir)

        excel_file_path = os.path.join(dir, fileId + ".xlsx")
        filename = os.path.basename(file)

        file_path = file
        if filename.split('.')[1]=="doc":
            # if filename =='00004749620130730095958.doc':
            #     print(filename)
            docData = psg.preprocessDoc(file_path)

            cldocData = ui.clean(docData)

            data= de._ExtractTitleNew(cldocData,fileId,filename)
            for d in data:
                d["FileId"]= fileId
                d["FileName"] = filename
                demo_data = ui.getDemographics(excel_file_path)
                d['docdate'] = ui.getDate(excel_file_path, filename)
                vit_data = ui.getVitals(excel_file_path, d['docdate'])

                # data = ui.mergeDict(data,demo_data)
                try:

                    d = {**d, **demo_data}
                    d = {**d, **vit_data}
                except:
                    pass
            records.append(data)
            continue
            data["FileId"]= fileId
            data["FileName"] = filename

            demo_data = ui.getDemographics(excel_file_path)
            for key in demo_data:
                d[key] = demo_data[key]
            data['docdate'] = ui.getDate(excel_file_path,filename)

            vit_data = ui.getVitals(excel_file_path,data['docdate'])
            for key in vit_data:
                d[key] = demo_data[key]

            # data = ui.mergeDict(data,demo_data)
            # try:
            #
            #     data = {**data, **demo_data}
            #     data = {**data,**vit_data}
            # except:
            #     pass

            # data["Date"] = row[0]
            records.append(data)




        if filename.split('.')[1] == "cmp":
            if filename=="00000004749620161213084427.cmp":
                print(filename)
            imgData = psg.preprocessImage(file_path.replace('cmp','tif'))
            climgData = ui.clean(imgData)
            data =  de._ExtractTitleNew(climgData,fileId,filename)
            for d in data:
                d["FileId"]= fileId
                d["FileName"] = filename

                demo_data = ui.getDemographics(excel_file_path)
                for key in demo_data:
                    d[key]= demo_data[key]

                d['docdate'] = ui.getDate(excel_file_path, filename)

                vit_data = ui.getVitals(excel_file_path, d['docdate'])
                for key in vit_data:
                    d[key]= demo_data[key]
            # data = ui.mergeDict(data,demo_data)
            #     try:
            #         d = {**d, **demo_data}
            #         d = {**d, **vit_data}
            #     except Exception as ex:
            #         print(ex)
            #         pass
            records.append(data)
            continue
            # data["FileId"]= fileId
            # data["FileName"]= filename
            # demo_data = ui.getDemographics(excel_file_path)
            # data['docdate'] = ui.getDate(excel_file_path, filename)
            # vit_data = ui.getVitals(excel_file_path, data['docdate'])
            # # data = ui.mergeDict(data,demo_data)
            # try:
            #     data = {**data, **demo_data}
            #     data = {**data,**vit_data}
            # except Exception as ex:
            #     print(ex)
            #     pass
        # data["Date"] = row[0]
            records.append(data)

    return records


def runMongoCommand(command):
    db.command({ "mapreduce":"Records_New",
      "map":'''function(){
          for(var key in this){
              if(key.length<30){


              emit(key,key);}
          }
      }''',
      "reduce":'function(key,stuff){return key;}',
      "out":"Records_New"+"_keys"})
numeric_keys = ["patient #","patient id","ht","wt","p","patient"]
def processNumericObjects(key,data):
    num = re.findall(r'\d+',data)
    outDict={}
    if len(num)>0:

        data= data.replace(num[0],num[0]+"^")
        dat = data.split("^")
        outDict[key]=dat[0]
        outDict["background"]=dat[1]
    else:
        outDict[key]=""
    # for dat in data:
    #     print(dat)
    return outDict




def PostProcessData():
    filenames  = collection.find({'FileName':{'$exists':'true'}},{'FileName':1})
    files = list(filenames)
    df = pd.DataFrame(files)
    files = df.FileName.unique()
    # filesss = collection.aggregate([{"$unwind": "$FileName"},{"$group": {"_id": "$FileName", "count": {"$sum": 1}}},{"$projection":"$$root"}])
    Data = []
    for file in files:

        fileInfo = collection.find({'FileName':file})
        fileDict = {}
        extention = file.split('.')[1]



        if extention == "cmp":
            print("cmp")
            for cur in fileInfo:
                if "date" in fileDict and fileDict["date"] == cur["date"]:
                    pass
                else:
                    Data.append(fileDict)

                    fileDict = {}
                    try:

                        fileDict["date"]=cur["date"]
                    except Exception as ex:
                        print(ex)
                # fileDict["date"]=cur["date"]
                for key in cur:
                    if key =="review of systems":
                        ros= ["constitutional","integumentary","ears, nose, throat, neck, mouth","eyes","cardiovascular","respiratory","gastrointestinal","genitourinary","neurologic","hematological","musculoskeletal"]
                        fileDict[key]=""
                        for i in ros:
                            if i in cur:
                                fileDict[key]=fileDict[key]+i+":"+cur[i]
                        continue

                    if key=="date":


                        print("date")
                    else:
                        if key.lower() in numeric_keys:
                            print(key)
                            numericData = processNumericObjects(key,cur[key])
                            print(numericData)
                        else:

                            fileDict[key]= cur[key]
            Data.append(fileDict)


        if extention =="doc":
            print("doc")
            for cur in fileInfo:
                for key in cur:
                    if key =="review of systems":
                        ros= ["constitutional","integumentary","ears, nose, throat, neck, mouth","eyes","cardiovascular","respiratory","gastrointestinal","genitourinary","neurologic","hematological","musculoskeletal"]
                        fileDict[key]=""
                        for i in ros:
                            if i in cur:
                                fileDict[key]=fileDict[key]+i+":"+cur[i]
                        continue
                    if key=="date" and "date" in fileDict:
                        if fileDict["date"]==cur[key]:
                            pass
                        else:
                            try:
                                test = ui.getDateTime(fileDict["date"])<ui.getDateTime(cur["date"])
                            except Exception as ex:
                                fileDict[key]=cur[key]
                                continue

                                print(ex)
                            if ui.getDateTime(fileDict["date"])<ui.getDateTime(cur["date"]):
                                fileDict["dob"]= fileDict["date"]
                                fileDict["date"]= cur["date"]
                            else:


                                fileDict["dob"]=cur[key]
                    else:
                        if key.lower() in numeric_keys:
                            print(key)
                            numericData = processNumericObjects(key,cur[key])
                            for key in numericData:
                                fileDict[key]=numericData[key]
                        else:

                            fileDict[key]= cur[key]


            Data.append(fileDict)
                    # fileDict[key]=cur[key]


        # for cur in fileInfo:
        #     for key in cur:
        #         if key not in fileDict:
        #
        #             fileDict[key]= cur[key]
        #         else:
        #             print(key)




    return Data



# save records to database
if __name__=="__main__":
    rec_res = {}
    # processExcels.readExcel(excel_files)
    # PostProcessData()


    records = process(files)
    for i in records:

        collection.insert_many(i)



    # runMongoCommand("Tes")
    finalData =PostProcessData()
    for i in finalData:
        if i:

            fi_collection.insert(i)










