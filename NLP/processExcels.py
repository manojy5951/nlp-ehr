import pandas as pd
from pymongo import MongoClient
import xlrd
import os



client = MongoClient()
db = client.nlp_ehr
collection = db.test
pd.set_option('display.max_columns', None)  # or 1000
pd.set_option('display.max_rows', None)  # or 1000
pd.set_option('display.max_colwidth', -1)
def readExcel(files):




    for file in files:
        dir = os.path.dirname(file)
        pId  = os.path.basename(dir)



        excel = pd.ExcelFile(file)
        for sheet in excel.sheet_names:
            data = []
            df = pd.read_excel(file,sheet)

            for i in df.itertuples():
                data_dict = {}
                for index,j in enumerate(i):
                    if index==0:
                        continue
                    data_dict[df.columns[index-1].replace('.','_')]=str(j)
                    data_dict['FileId'] =pId

                data.append(data_dict)
                # print(i)
            try:

                db[sheet].insert_many(data)
            except Exception as ex:
                print(ex)




if __name__=="__main__":
    readExcel([])