import nltk
punctuations = [':',",",".","/","?"]
from nltk import word_tokenize
import constant as cnt
import re
import pandas as pd
import datetime
import utilities as ui
from dateutil.parser import parse
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))
import DE
# from nltk.tag.stanford import StanfordNERTagger
# st = StanfordNERTagger('/home/manojy/Desktop/ManojY/nltk_ner/stanford-ner-2018-10-16/classifiers/english.all.3class.distsim.crf.ser.gz',
# 					   '/home/manojy/Desktop/ManojY/nltk_ner/stanford-ner-2018-10-16/stanford-ner.jar',
# 					   encoding='utf-8')
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass


    return False


def _stemmer(listarray=[]):
    words=[]
    for x in listarray:
        # s=[ps.stem(x) for x in nltk.word_tokenize(x)]
        # s = nltk.word_tokenize(x)
        # words.append(' '.join(s))
        words.append(x.lower())
    return words



def _preProcessSent(word_tokens,bag_of_titles):
    # print(word_tokens)
    # print(bag_of_titles)
    # ss = ' '.join(word_tokens)
    # for i in bag_of_titles:
    #     # res = re.findall(i,ss)
    #     ss= ss.replace(i,'~ '+i+' ~')

    for index,word in enumerate(word_tokens):
        if index==0:
            word_tokens[0]="; "+word

        try:

            if word.lower() in bag_of_titles and word_tokens[index+1]==":":
                # print(word)
                if is_number(word):
                    continue



                word_tokens[index]=";  "+word+" ;"

            if word in punctuations:
                word_tokens[index] =" "
        except Exception as ex:
            print(ex)
    # print(ss)




    return ' '.join(word_tokens)

def _ExtractTitleNew(processData,fileId,filename):
    data_temp = {}
    all_rows = []
    processData = ' '.join(processData)

    p = re.compile(r'(\d+/\d+/\d+)')
    match = p.findall(processData)
    # if match[0]=='1/19/2017':
    #     print("match")
    try:

        datestemp = []
        if len(match)>=1:
            for i in match:
                try:
                    parse(i)
                    datestemp.append(i)
                    # return True
                except ValueError:
                    print("error")
                    # return False
        datestemp = pd.Series(datestemp)
        if len(datestemp.unique())>1:

            data_temp["datetemp"]=datestemp.max()
            # data_temp["tempdob"]= datestemp.min()
        else:
            data_temp["datetemp"] = datestemp[0]
    except:
        pass
    # print(match.group(1))
    # print(match.groups())


    data_temp = DE.processData(processData,filename)
    return  data_temp
    processData = processData.replace(':',' : ')

    processData = ui.markExceptions(processData)

    word_tokens = nltk.word_tokenize(processData)
    # print(word_tokens)
    word_tags = nltk.pos_tag(word_tokens)

    bag_of_titles = []
    for index, word in enumerate(word_tags):
        # print(word)
        # if word[0]=="$":
        #     print(word)

        if word[0] == ":" and word_tags[index-1][0].lower() not in stop_words and word[0] !="$" and word[0]!="#":

            wrds = []
            while word_tags[index-1][0].isupper() or word_tags[index-1][0][0].isupper() and word_tags[index-1][0]!="$":

                wrds.append(word_tags[index-1][0])
                index-=1
            if len(wrds)==0:
                if word_tags[index-1][0]=="by":
                    wrds.append(word_tags[index-1][0])
                    wrds.append(word_tags[index-2][0])

                else:

                    wrds.append(word_tags[index-1][0])
            # print(wrds)
            wrds =  wrds[::-1]

            for i,w in enumerate(wrds):
                if w=="to" or w=="To" or w=="TO" or is_number(w):
                    del wrds[i]
                # if w=='by':
                #     w = wrds[i-1]+' by'
            if len(wrds)>0 and len(wrds)<2:


                bag_of_titles.append(''.join(wrds).lower())
            if len(wrds)>1:
                processData = processData.replace(' '.join(wrds),'_'.join(wrds))
                bag_of_titles.append('_'.join(wrds).lower())

            # bag_of_titles.append(word_tags[index - 1][0].lower())
            # print(False)

    bag_of_titles = bag_of_titles + _stemmer(cnt.attribute)
    # if "$" in bag_of_titles:
    #     bag_of_titles.remove("$")
    # if "#" in bag_of_titles:
    #     bag_of_titles.remove("#")
    bag_of_titles= [x for x in bag_of_titles if x != "$"]
    bag_of_titles = [x for x in bag_of_titles if x != "#"]
    word_tokens = word_tokenize(processData)
    word_tokens = [ i.lower() for i in word_tokens ]
    # preprocessing the sentence to remove punctuations and stop words and marking the title words
    processedSent = _preProcessSent(word_tokens, bag_of_titles)


    # wrd_tkn = word_tokenize(processedSent)

    # ress = processedSent.split('$')
    res = processedSent.split(';')
    res = [i.strip() for i in res]
    isnull =False
    for i, wrd in enumerate(res):


        if wrd in bag_of_titles:
            try:

                if is_number(wrd):
                    pass
                else:

                    if wrd.replace('.', '').strip().lower()=="":
                        # match = re.match(r'^[2-9]\d{2}-\d{3}-\d{4}$')
                        match = re.search(r'^[2-9]\d{2}-\d{3}-\d{4}$', res[i + 1].strip().lower())
                        match1 = re.search(r'((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}',res[i + 1].strip().lower())
                        matchdate = re.search(r'(\d+/\d+/\d+)', processData)

                        try:
                            dt = matchdate.group()
                            res["date"] = dt
                            res[i+1]=res[i + 1].strip().lower().replace(dt,'')
                        except:
                            pass


                        isAddress = False
                        try:
                            if match1.group() or match.group():
                                isAddress =True
                        except:
                            pass

                        if not isAddress:

                            data_temp["diagnosis"] = res[i + 1].strip().lower().split(fileId)[1].replace('$','')
                    else:
                        text_value = res[i + 1].strip().lower().split('$')[0]
                        key_value = wrd.replace('.','').strip().lower()
                        # if key_value=="bp":
                        #     data_temp["systolic"]= text_value.split("/")[0]
                        #     data_temp["diastolic"]= text_value.split("/")[1]
                        #     continue
                        if key_value =="$" or key_value =="#":
                            continue
                        if key_value in cnt.ros:
                            if "review_of_systems" in data_temp:
                                data_temp["review_of_systems"] = data_temp["review_of_systems"]+key_value+":"+text_value
                                continue

                        if key_value=="bp":
                            data_temp["systolic"]= text_value.split("/")[0]
                            data_temp["diastolic"]= text_value.split("/")[1]
                            continue
                        if key_value=="date":
                            try:
                                matchdate = re.search(r'(\d+/\d+/\d+)', text_value)
                                data_temp[key_value]= matchdate.group()
                                continue
                            except:
                                print("error")
                        if key_value=="nkda":
                            if "allergies" in data_temp:

                                data_temp["allergies"]=data_temp["allergies"]+text_value
                            else:
                                data_temp["allergies"]= text_value
                            continue
                        if key_value=="follows":
                            if "review_of_systems" in data_temp:

                                data_temp["review_of_systems"]=data_temp["review_of_systems"]+"  "+text_value
                            else:
                                data_temp["review_of_systems"]=text_value
                            continue
                        if key_value=="p":

                            data_temp[key_value] = text_value.split(" ")[0]
                            text_value = text_value.replace(data_temp[key_value],"")
                            if "diagnosis" in key_value:

                                data_temp["diagnosis"] = data_temp["diagnosis"]+"  "+' '.join(text_value.split(" ")[0:])
                            else:
                                data_temp["diagnosis"]= ' '.join(text_value.split(" ")[0:])
                        else:
                            if key_value in data_temp:

                                data_temp[key_value] = data_temp[key_value]+"  "+text_value
                            else:
                                data_temp[key_value] = text_value
            except Exception as ex:

                # data_temp[res[i+1]]= res[i+2]
                isnull=True

                # print(ex)
        else:

            if isnull:

                print(i)
                key_value = wrd.replace('.', '').strip().lower()

                if key_value=="$" or key_value =="#" or key_value=="$ $":
                    continue
                if i%2 ==1:

                    try:
                        text_value = res[i + 1].strip().lower().split('$')[0]
                        if key_value=="bp":
                            data_temp["systolic"]= text_value.split("/")[0]
                            data_temp["diastolic"]= text_value.split("/")[1]
                            continue
                        if key_value in cnt.ros:
                            if "review_of_systems" in data_temp:
                                data_temp["review_of_systems"] = data_temp["review_of_systems"]+ key_value+":"+text_value
                                continue

                        dt = re.search(r'(\d+/\d+/\d+)', text_value)
                        st = dt.group()
                        if "diagnosis" in data_temp:

                            data_temp["diagnosis"]= data_temp["diagnosis"]+"  "+text_value.split(st)[1]
                        else:
                            data_temp["diagnosis"]= text_value.split(st)[1]

                        text_value = text_value.split(st)[0]


                    except Exception as ex:
                        pass

                    if key_value in data_temp:

                        data_temp[key_value] =data_temp[key_value]+"  "+ text_value
                    else:
                        data_temp[key_value]= text_value

            # pass

    all_rows.append(data_temp)
    return  data_temp



